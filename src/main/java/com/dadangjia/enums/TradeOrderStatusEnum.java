package com.dadangjia.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dadangjia 2018.4.17
 */
public enum TradeOrderStatusEnum {

    //待付款 已付款 取消
    INIT("初始化",1), //在用户交易订单列表中看到的就是 待付款 状态   进行中的订单 点击购买下单后
    PAYED("已付款",2) ,//已付款
    CANCEL("订单取消",3), //订单取消  交易关闭
    RELEASE("释放币",4),//释放币
    COMPLETE("订单完成",5),//已完成

    ;






    private String name;
    private int value;

    private TradeOrderStatusEnum(String name, int value) {
        this.name = name;
        this.value = value;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public static TradeOrderStatusEnum statusOfEnum(int status){
        for(TradeOrderStatusEnum num : values() ){
            if(status == num.getValue()){
                return num;
            }
        }
        return null;
    }

    /**
     * 根据订单状态 找出 可以允许修改的订单状态
     * @param orderStatus
     * @return
     */
    public static List<Integer> allowUpdateStatus(int orderStatus){
        List<Integer> statusList = new ArrayList<Integer>();
        if(orderStatus == INIT.getValue()){
            statusList.add(PAYED.getValue());
            statusList.add(CANCEL.getValue());
            return statusList;
        }
        if(orderStatus == CANCEL.getValue()){
            return statusList;
        }
        if(orderStatus == PAYED.getValue()){
            statusList.add(RELEASE.getValue());
            return statusList;
        }
        if(orderStatus == RELEASE.getValue()){
            statusList.add(COMPLETE.getValue());
            return statusList;
        }
        if(orderStatus == COMPLETE.getValue()){
            return statusList;
        }
        return statusList;
    }


    /**
     * 卖家根据订单号获取
     * @param orderStatus
     * @return
     */
    public static String statusToDescForSeller(int orderStatus){

        if(orderStatus == INIT.getValue()){
            return "待释放";
        }
        if(orderStatus == CANCEL.getValue()){
            return "订单取消";
        }
        if(orderStatus == PAYED.getValue()){
            return "待释放";
        }
        if(orderStatus == RELEASE.getValue()){
            return "已释放";
        }
        if(orderStatus == COMPLETE.getValue()){
            return "已完成";
        }
        return "";
    }

    /**
     * 买家根据订单号获取
     * @param orderStatus
     * @return
     */
    public static String statusToDescForBuyer(int orderStatus){

        if(orderStatus == INIT.getValue()){
            return "待付款";
        }
        if(orderStatus == CANCEL.getValue()){
            return "订单取消";
        }
        if(orderStatus == PAYED.getValue()){
            return "已付款";
        }
        if(orderStatus == RELEASE.getValue()){
            return "已接收";
        }
        if(orderStatus == COMPLETE.getValue()){
            return "已完成";
        }
        return "";
    }
}
