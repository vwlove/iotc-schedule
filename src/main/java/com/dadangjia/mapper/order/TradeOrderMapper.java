package com.dadangjia.mapper.order;

import com.dadangjia.entity.order.TradeOrder;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Description:购买交易
 * dadangjia 2018.4.17
 */
@Mapper
public interface TradeOrderMapper {

    public List<TradeOrder> queryExpireOrders();

    public int updateExpireOrders(@Param("ids") List<Integer> ids, @Param("status") Integer status);




}
