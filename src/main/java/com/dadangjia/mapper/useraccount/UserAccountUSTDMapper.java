package com.dadangjia.mapper.useraccount;


import com.dadangjia.entity.useraccount.UserAccountUSTD;
import org.apache.ibatis.annotations.Mapper;

/**
 * Description:
 * dadangjia 2018.4.17
 */
@Mapper
public interface UserAccountUSTDMapper {
    public UserAccountUSTD getByUserId(String userId);

    public int add(UserAccountUSTD userAccount);

    public UserAccountUSTD getByUrl(String url);

    public int update(UserAccountUSTD userAccount);

}
