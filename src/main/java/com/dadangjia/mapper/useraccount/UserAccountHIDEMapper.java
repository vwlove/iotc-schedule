package com.dadangjia.mapper.useraccount;


import com.dadangjia.entity.useraccount.UserAccountHIDE;
import org.apache.ibatis.annotations.Mapper;

/**
 * Description:
 * DATE:2018-03-15
 * TIME:上午7:25
 */
@Mapper
public interface UserAccountHIDEMapper {
    public UserAccountHIDE getByUserId(String userId);

    public UserAccountHIDE getByUrl(String url);

    public int add(UserAccountHIDE userAccount);

    public int update(UserAccountHIDE userAccount);

}
