package com.dadangjia.mapper.useraccount;


import com.dadangjia.entity.useraccount.UserAccountLTC;
import org.apache.ibatis.annotations.Mapper;

/**
 * Description:
 * dadangjia 2018.4.17
 */
@Mapper
public interface UserAccountLTCMapper {
    public UserAccountLTC getByUserId(String userId);

    public UserAccountLTC getByUrl(String url);

    public int add(UserAccountLTC userAccount);

    public int update(UserAccountLTC userAccount);

}
