package com.dadangjia.mapper.useraccount;


import com.dadangjia.entity.useraccount.UserHide;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * Created by dadangjia 2018.4.17
 */
@Mapper
public interface UserHideMapper {

    public List<UserHide> getAllList();

    public int addHide(UserHide userHide);

    public int updateUserHideValid(UserHide userHide);

    public UserHide getUserHideByUserIds(Map<String, String> map);
}
