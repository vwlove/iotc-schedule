package com.dadangjia.mapper.useraccount;


import com.dadangjia.entity.useraccount.UserAccountETH;
import org.apache.ibatis.annotations.Mapper;

/**
 * Description:
 * dadangjia 2018.4.17
 */
@Mapper
public interface UserAccountETHMapper {
    public UserAccountETH getByUserId(String userId);

    public UserAccountETH getByUrl(String url);

    public int add(UserAccountETH userAccount);

    public int update(UserAccountETH userAccount);

}
