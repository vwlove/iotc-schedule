package com.dadangjia.mapper.useraccount;


import com.dadangjia.entity.useraccount.UserAccountBTC;
import org.apache.ibatis.annotations.Mapper;

/**
 * Description:
 * dadangjia 2018.4.17
 */
@Mapper
public interface UserAccountBTCMapper {
    public UserAccountBTC getByUserId(String userId);

    public UserAccountBTC getByUrl(String url);

    public int add(UserAccountBTC userAccountBTC);

    public int update(UserAccountBTC userAccountBTC);

}
