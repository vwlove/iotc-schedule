package com.dadangjia.entity.useraccount;

import lombok.Data;

import java.util.Date;

/**
 *
 * Description:  用户比特币账户
 * dadangjia 2018.4.17
 */
@Data
public class UserAccountBTC {

    //主键id
    private int  id;
    //用户id
    private String userId;
    //用户当前币资产,最小单位实际需要除以100000000也就是1后面8个零
    private long balance;
    //当前币冻结资产,最小单位实际需要除以100000000也就是1后面8个零
    private long frozenBalance;
    //充值地址
    private String rechargeUrl;
    //创建时间
    private Date createTime;
    //修改时间
    private Date operaTime;
    //是否有效
    private int valid;
    //乐观锁
    private int version;


}
