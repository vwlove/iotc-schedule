package com.dadangjia.entity.useraccount;

import lombok.Data;

import java.util.Date;

/**
 * 用户屏蔽其他用户表
 */
@Data
public class UserHide {

    private int id;

    private String userId;

    private String hideUserId;

    private Date createTime;

    private Date operaTime;

    private int valid;

}
