package com.dadangjia.entity;

import lombok.Data;

/**
 * Description:分页
 * dadangjia 2018.4.17
 */
@Data
public class Paging {

    private int start;
    private int pageSize;

    public Paging() {
    }


}
