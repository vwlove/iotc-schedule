package com.dadangjia.entity.order;

import com.dadangjia.entity.Paging;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 订单交易表
 * Created by dadangjia 2018.4.17
 */
@Data
public class TradeOrder extends Paging {

    //主键id
    private int id;
    //订单号
    private String orderNo;
    //发布出售的广告号
    private String advertNo;

    //sell or buy 是售出 还是购买 用于查广告
    private String tradeType;
    //币种
    private String curTypeId;
    //成交价格
    private long transactionPrice;
    //成交数量-按照平台最小单位
    private long number;
    //卖家
    private String sellUserId;
    //买家
    private String buyUserId;
    //订单过期时
    private Date expireTime;
    //订单创建时间
    private Date createTime;
    //订单修改时间
    private Date operaTime;
    //订单状态  交易订单状态0-订单完成,10-初始化订单,1-订单取消,2-订单超时,3-确认(卖方释放币)
    private int orderStatus;
    // 订单金额
    private int amount;
    //买家是否已付款
    private int isPayment;

    //支付方式
    private int payMethodId;

    //以上对应的是数据库名称
    //下面是查询需要用到的数据
    private List<Integer> orderStatusList;


}
