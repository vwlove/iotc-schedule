package com.dadangjia.task;

import com.dadangjia.service.order.IOrderService;
import com.dadangjia.service.useraccount.IUserAccountService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created by dadangjia on 2018/4/17.
 */
@Component
public class CheckOrderExpireTask {
    @Resource
    private IOrderService orderService;


    /**
     * 定时任务，每隔5 分钟检查订单是否过期
     */
    @Scheduled(cron = "0/5 * * * * ? ")
    public void deleteAllTempClob() {
        orderService.checkOrderExpire();
        System.out.println("---->>schedule");
    }

}
