package com.dadangjia.service.useraccount;

/**
 * Created by dadangjia on 2018/4/18.
 */
public interface IUserAccountService {
    /**
     * 下单冻结卖家币
     *
     * @param sellUserId 卖家userId
     * @param curTypeId  币种
     * @param curNum     冻结币数量
     * @return
     */
    public boolean frozenForTradeOrder(String sellUserId, String curTypeId, long curNum);


    /**
     * 解冻
     *
     * @param userId
     * @param curTypeId 币种
     * @param curNum    冻结币数量
     * @return
     */
    public boolean unFrozenForTradeOrder(String userId, String curTypeId, long curNum);

    /**
     * 下单冻结卖家币
     *
     * @param fromUserId fromUserId
     * @param toUserId   toUserId
     * @param curTypeId  币种
     * @param curNum     冻结币数量
     * @return
     */
    public boolean transfer(String fromUserId, String toUserId, String curTypeId, long curNum);

    /**
     * 入账 有乐观锁 需要调用方重试 添加事务
     *
     * @param url
     * @param curTypeId
     * @param curNum
     * @return
     */
    public boolean gathering(String url, String curTypeId, long curNum);
}
