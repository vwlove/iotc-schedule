package com.dadangjia.service.useraccount.impl;

import com.dadangjia.entity.useraccount.*;
import com.dadangjia.mapper.useraccount.*;
import com.dadangjia.service.useraccount.IUserAccountService;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by dadangjia on 2018/4/18.
 */
@Service
public class UserAccountService implements IUserAccountService {
    @Resource
    private UserAccountBTCMapper userAccountBTCMapper;

    @Resource
    private UserAccountETHMapper userAccountETHMapper;

    @Resource
    private UserAccountHIDEMapper userAccountHIDEMapper;

    @Resource
    private UserAccountLTCMapper userAccountLTCMapper;

    @Resource
    private UserAccountUSTDMapper userAccountUSTDMapper;

    public boolean frozenForTradeOrder(String sellUserId, String curTypeId, long curNum) {
        if ("eth".equalsIgnoreCase(curTypeId)) {
            UserAccountETH account = userAccountETHMapper.getByUserId(sellUserId);
            if (account.getBalance() - account.getFrozenBalance() - curNum < 0) return false;
            account.setFrozenBalance(account.getFrozenBalance() + curNum);
            if (userAccountETHMapper.update(account) == 1) return true;
        } else if ("btc".equalsIgnoreCase(curTypeId)) {
            UserAccountBTC account = userAccountBTCMapper.getByUserId(sellUserId);
            if (account.getBalance() - account.getFrozenBalance() - curNum < 0) return false;
            account.setFrozenBalance(account.getFrozenBalance() + curNum);
            if (userAccountBTCMapper.update(account) == 1) return true;
        } else if ("hide".equalsIgnoreCase(curTypeId)) {
            UserAccountHIDE account = userAccountHIDEMapper.getByUserId(sellUserId);
            if (account.getBalance() - account.getFrozenBalance() - curNum < 0) return false;
            account.setFrozenBalance(account.getFrozenBalance() + curNum);
            if (userAccountHIDEMapper.update(account) == 1) return true;
        } else if ("ltc".equalsIgnoreCase(curTypeId)) {
            UserAccountLTC account = userAccountLTCMapper.getByUserId(sellUserId);
            if (account.getBalance() - account.getFrozenBalance() - curNum < 0) return false;
            account.setFrozenBalance(account.getFrozenBalance() + curNum);
            if (userAccountLTCMapper.update(account) == 1) return true;
        } else if ("ustd".equalsIgnoreCase(curTypeId)) {
            UserAccountUSTD account = userAccountUSTDMapper.getByUserId(sellUserId);
            if (account.getBalance() - account.getFrozenBalance() - curNum < 0) return false;
            account.setFrozenBalance(account.getFrozenBalance() + curNum);
            if (userAccountUSTDMapper.update(account) == 1) return true;
        }
        return false;
    }

    @Override
    public boolean unFrozenForTradeOrder(String userId, String curTypeId, long curNum) {
        if ("eth".equalsIgnoreCase(curTypeId)) {
            UserAccountETH account = userAccountETHMapper.getByUserId(userId);
            if (account.getFrozenBalance() - curNum < 0) return false;
            account.setFrozenBalance(account.getFrozenBalance() - curNum);
            if (userAccountETHMapper.update(account) == 1) return true;
        } else if ("btc".equalsIgnoreCase(curTypeId)) {
            UserAccountBTC account = userAccountBTCMapper.getByUserId(userId);
            if (account.getFrozenBalance() - curNum < 0) return false;
            account.setFrozenBalance(account.getFrozenBalance() - curNum);
            if (userAccountBTCMapper.update(account) == 1) return true;
        } else if ("hide".equalsIgnoreCase(curTypeId)) {
            UserAccountHIDE account = userAccountHIDEMapper.getByUserId(userId);
            if (account.getFrozenBalance() - curNum < 0) return false;
            account.setFrozenBalance(account.getFrozenBalance() - curNum);
            if (userAccountHIDEMapper.update(account) == 1) return true;
        } else if ("ltc".equalsIgnoreCase(curTypeId)) {
            UserAccountLTC account = userAccountLTCMapper.getByUserId(userId);
            if (account.getFrozenBalance() - curNum < 0) return false;
            account.setFrozenBalance(account.getFrozenBalance() - curNum);
            if (userAccountLTCMapper.update(account) == 1) return true;
        } else if ("ustd".equalsIgnoreCase(curTypeId)) {
            UserAccountUSTD account = userAccountUSTDMapper.getByUserId(userId);
            if (account.getFrozenBalance() - curNum < 0) return false;
            account.setFrozenBalance(account.getFrozenBalance() - curNum);
            if (userAccountUSTDMapper.update(account) == 1) return true;
        }
        return false;
    }

    @Override
    public boolean transfer(String fromUserId, String toUserId, String curTypeId, long curNum) {
        if ("eth".equalsIgnoreCase(curTypeId)) {
            UserAccountETH account = userAccountETHMapper.getByUserId(fromUserId);
            if (account.getBalance() - account.getFrozenBalance() - curNum < 0) return false;

            account.setBalance(account.getBalance() - curNum);
            if (userAccountETHMapper.update(account) == 1) {
                UserAccountETH toAccount = userAccountETHMapper.getByUserId(toUserId);
                toAccount.setBalance(toAccount.getBalance() + curNum);
                return userAccountETHMapper.update(toAccount) == 1;
            }

        } else if ("btc".equalsIgnoreCase(curTypeId)) {


            UserAccountBTC account = userAccountBTCMapper.getByUserId(fromUserId);
            if (account.getBalance() - account.getFrozenBalance() - curNum < 0) return false;

            account.setBalance(account.getBalance() - curNum);
            if (userAccountBTCMapper.update(account) == 1) {
                UserAccountBTC toAccount = userAccountBTCMapper.getByUserId(toUserId);
                toAccount.setBalance(toAccount.getBalance() + curNum);
                return userAccountBTCMapper.update(toAccount) == 1;
            }
        } else if ("hide".equalsIgnoreCase(curTypeId)) {

            UserAccountHIDE account = userAccountHIDEMapper.getByUserId(fromUserId);
            if (account.getBalance() - account.getFrozenBalance() - curNum < 0) return false;
            account.setFrozenBalance(account.getFrozenBalance() - curNum);

            if (userAccountHIDEMapper.update(account) == 1) {
                UserAccountHIDE toAccount = userAccountHIDEMapper.getByUserId(toUserId);
                toAccount.setBalance(toAccount.getBalance() + curNum);
                return userAccountHIDEMapper.update(toAccount) == 1;
            }
        } else if ("ltc".equalsIgnoreCase(curTypeId)) {
            UserAccountLTC account = userAccountLTCMapper.getByUserId(fromUserId);
            if (account.getBalance() - account.getFrozenBalance() - curNum < 0) return false;

            account.setBalance(account.getBalance() - curNum);
            if (userAccountLTCMapper.update(account) == 1) {
                UserAccountLTC toAccount = userAccountLTCMapper.getByUserId(toUserId);
                toAccount.setBalance(toAccount.getBalance() + curNum);
                return userAccountLTCMapper.update(toAccount) == 1;
            }
        } else if ("ustd".equalsIgnoreCase(curTypeId)) {
            UserAccountUSTD account = userAccountUSTDMapper.getByUserId(fromUserId);
            if (account.getBalance() - account.getFrozenBalance() - curNum < 0) return false;

            account.setBalance(account.getBalance() - curNum);
            if (userAccountUSTDMapper.update(account) == 1) {
                UserAccountUSTD toAccount = userAccountUSTDMapper.getByUserId(toUserId);
                toAccount.setBalance(toAccount.getBalance() + curNum);
                return userAccountUSTDMapper.update(toAccount) == 1;
            }
        }
        return false;
    }

    //收款入账
    @Override
    public boolean gathering(String url, String curTypeId, long curNum) {

        if ("eth".equalsIgnoreCase(curTypeId)) {
            UserAccountETH account = userAccountETHMapper.getByUrl(url);
            account.setBalance(account.getBalance() + curNum);
            if (userAccountETHMapper.update(account) == 1) return true;
        } else if ("btc".equalsIgnoreCase(curTypeId)) {
            UserAccountBTC account = userAccountBTCMapper.getByUrl(url);
            account.setBalance(account.getBalance() + curNum);
            if (userAccountBTCMapper.update(account) == 1) return true;
        } else if ("hide".equalsIgnoreCase(curTypeId)) {
            UserAccountHIDE account = userAccountHIDEMapper.getByUrl(url);
            account.setBalance(account.getBalance() + curNum);
            if (userAccountHIDEMapper.update(account) == 1) return true;
        } else if ("ltc".equalsIgnoreCase(curTypeId)) {
            UserAccountLTC account = userAccountLTCMapper.getByUrl(url);
            account.setBalance(account.getBalance() + curNum);
            if (userAccountLTCMapper.update(account) == 1) return true;
        } else if ("ustd".equalsIgnoreCase(curTypeId)) {
            UserAccountUSTD account = userAccountUSTDMapper.getByUrl(url);
            account.setBalance(account.getBalance() + curNum);
            if (userAccountUSTDMapper.update(account) == 1) return true;
        }
        return false;
    }
}
