package com.dadangjia.service.order;



/**
 * Created by dadangjia on 2018/4/18.
 */
public interface IOrderService {



    /**
     * 检查订单是否过期，对过期的订单进行状态更新
     *
     */
    public void checkOrderExpire();
}
