package com.dadangjia.service.order.impl;

import com.dadangjia.entity.order.TradeOrder;
import com.dadangjia.enums.TradeOrderStatusEnum;
import com.dadangjia.mapper.order.TradeOrderMapper;
import com.dadangjia.service.order.IOrderService;
import com.dadangjia.service.useraccount.IUserAccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by dadangjia 2018.4.17
 */
@Service
public class OrderService implements IOrderService {


    private static final Logger logger = LoggerFactory.getLogger(OrderService.class);


    @Resource
    private TradeOrderMapper tradeOrderMapper;
    @Resource
    private IUserAccountService userAccountService;


    public void checkOrderExpire() {
        List<TradeOrder> list = tradeOrderMapper.queryExpireOrders();
        if (!CollectionUtils.isEmpty(list)) {
            List<Integer> ids = list.stream().map(TradeOrder::getId).collect(Collectors.toList());
            logger.info("queryExpireOrders:{} ", ids);
//        list.forEach(tradeOrder -> {
//            userAccountService.unFrozenForTradeOrder(tradeOrder.getSellUserId(), tradeOrder.getCurTypeId(), tradeOrder.getAmount());
//            logger.info("userAccountService.unFrozenForTradeOrder userId:{},curTyep:{},amount:{} ",
//                    tradeOrder.getSellUserId(), tradeOrder.getCurTypeId(), tradeOrder.getAmount());
//        });
//        int sum = tradeOrderMapper.updateExpireOrders(ids, TradeOrderStatusEnum.CANCEL.getValue());
//        logger.info("checkOrderExpire:{} ", sum);
        }

    }
}
